import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';
import { Empresa } from '../models/empresa.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( private globalService: GlobalService) {
   }

  ngOnInit() {
    this.globalService.resultSearch = new Array<Empresa>();
    this.globalService.textSearch = "";
  }


}
