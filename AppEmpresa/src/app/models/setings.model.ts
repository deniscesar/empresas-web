export class AppSettings {
    public AppName: string;
    public ServiceUrl: string;

    constructor(){
        this.AppName = "AppEmpresas";
        this.ServiceUrl = "http://empresas.ioasys.com.br/api/v1/";
    }   
}