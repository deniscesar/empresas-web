export class Session
{
    public Uid: string;
    public Client: string;
    public AccessToken: string;
}