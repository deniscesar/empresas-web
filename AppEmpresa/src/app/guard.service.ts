import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";

import { GlobalService } from "./global.service";

@Injectable()
export class GuardService implements CanActivate{
    
    constructor(
        private globalService: GlobalService) {
    }

    canActivate(): boolean {
        return this.globalService.autenticado();
    }

}