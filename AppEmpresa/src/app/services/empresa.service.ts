import { Http, Headers, Response, RequestMethod, RequestOptions } from '@angular/http'
import { Injectable } from '@angular/core'
import 'rxjs/add/operator/topromise'
//Services
import { GlobalService } from '../global.service';
//Models
import { AppSettings } from '../models/setings.model'
import { Empresa } from '../models/empresa.model';

@Injectable()
export class EmpresaService {

    private settings: AppSettings;

    constructor(
        private http: Http,
        private globalService: GlobalService) {
            this.settings = new AppSettings();
    }

    public buscaEmpresa(id: number): Promise<Empresa> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('access-token', this.globalService.session.AccessToken);
        headers.append('client', this.globalService.session.Client);
        headers.append('uid', this.globalService.session.Uid);
        let options = new RequestOptions({ headers: headers });
        
        return this.http.get(this.settings.ServiceUrl + 'enterprises/' + id, options)
            .toPromise()
            .then(resp => resp.json(),
            (error) => {
                let body = error.json()
            })
    }

}