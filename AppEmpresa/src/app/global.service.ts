import { Http, Headers, Response, RequestMethod, RequestOptions } from '@angular/http'
import { Injectable } from '@angular/core';
import { Router } from '@angular/router'
import 'rxjs/add/operator/topromise'

import { AppSettings } from './models/setings.model';
import { Session } from './models/session.model';
import { Login } from './models/login.model';
import { Empresa } from './models/empresa.model';

@Injectable()
export class GlobalService {

    public settings: AppSettings;
    public session: Session;
    public textSearch: string;
    public resultSearch: Empresa[];


    constructor(private http: Http, private router: Router) {
        this.settings = new AppSettings();
        this.session = new Session();
        this.textSearch = '';
        this.resultSearch = new Array<Empresa>();
    }

    public login(dados: Login): Promise<any> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.settings.ServiceUrl + 'users/auth/sign_in', dados, options)
            .toPromise()
            .then((data) => {
                this.session.AccessToken = data.headers.get('access-token');
                this.session.Client = data.headers.get('client');
                this.session.Uid = data.headers.get('uid');
                this.router.navigate(['/home']);
            },(error) => {
                error.json();
            })
    }
    
    public Search() {
        this.resultSearch = new Array<Empresa>();
        if (this.textSearch.length >= 3) {
            const headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('access-token', this.session.AccessToken);
            headers.append('client', this.session.Client);
            headers.append('uid', this.session.Uid);
            let options = new RequestOptions({ headers: headers });
            
            this.http.get(this.settings.ServiceUrl + 'enterprises?name=' + this.textSearch, options)
            .subscribe((data) => {
                this.resultSearch = data.json()['enterprises'];
            }, (error) =>{
                console.log(error);
            })
        }
    }

    public autenticado(): boolean {
        if(this.session.AccessToken != null && this.session.Client != null && this.session.Uid != null){
            return true;
        }else{
            this.router.navigate(['/']);
            return false;            
        }
    }
    
}