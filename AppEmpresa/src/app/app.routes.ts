import { Routes, CanActivate } from '@angular/router'

import { HomeComponent } from './home/home.component'
import { LoginComponent } from './login/login.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { GuardService } from './guard.service';

export const ROUTES: Routes = [
    { path: '', component: LoginComponent },
    { path: 'home', component: HomeComponent, canActivate: [ GuardService ]},
    { path: 'empresa/:id', component: EmpresaComponent, canActivate: [ GuardService ] }
]