import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public isSearch: boolean;

  constructor(public globalService: GlobalService) {
    this.isSearch = false;
   }

  ngOnInit() {
  }

  public AtctiveSearch(){
    this.globalService.textSearch = '';
    if(this.isSearch == false){
      this.isSearch = true;
    }else{
      this.isSearch = false;
    }
  }

}
