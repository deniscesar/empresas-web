import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { GlobalService } from '../global.service';
import { Login } from '../models/login.model';
import { Alert } from '../../../node_modules/@types/selenium-webdriver';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public dados: Login;
  public info: boolean;

  constructor(private globalService: GlobalService, private router: Router) { 
    this.dados = new Login();
    this.info = false;
  }

  ngOnInit() {
  }

  public login() {    
    this.globalService.login(this.dados).then(response => {
      if(!response){
        this.dados = new Login();
        this.info = true;
      }
    })
  }
}
