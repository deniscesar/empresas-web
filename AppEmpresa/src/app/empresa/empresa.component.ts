import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../global.service';
import { EmpresaService } from "../services/empresa.service";
import { Empresa } from '../models/empresa.model';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css'],
  providers: [EmpresaService]
})
export class EmpresaComponent implements OnInit {

  public idEmpresa: number;
  public empresa: Empresa;

  constructor( private globalService: GlobalService,
    private activateRoute: ActivatedRoute,
    private empresaService: EmpresaService) {
      this.empresa = new Empresa();
      this.idEmpresa = this.activateRoute.snapshot.params['id'];
   }

  ngOnInit() {
    this.empresaService.buscaEmpresa(this.idEmpresa).then(response =>{
      this.empresa = response['enterprise'];
    })
  }

}
